class IncomeCalculator
  def self.calculate(investment_amount)
    expected = investment_amount.to_f * Settings.expected_income_rate
    actual = calculate_actual_income(investment_amount.to_f)
    { expected: expected.round(2), actual: actual }
  end

  private

  def self.calculate_actual_income(investment_amount)
    rate = Payment.sum(:summary_amount).fdiv(Loan.sum(:amount))
    result = rate * investment_amount unless rate.nan? or rate.infinite?
    result.to_f.round(2)
  end
end
