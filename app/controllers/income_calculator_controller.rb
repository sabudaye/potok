require 'income_calculator' # rails 5 heroku fix
class IncomeCalculatorController < ApplicationController
  def index
  end

  def new
    @result = ::IncomeCalculator.calculate(permit_params[:investment_amount])
    render :index
  end

  private

  def permit_params
    params.permit(:investment_amount)
  end
end
