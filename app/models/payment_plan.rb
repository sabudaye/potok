# == Schema Information
#
# Table name: payment_plans
#
#  id                :integer          not null, primary key
#  loan_id           :integer
#  root_amount       :decimal(, )      not null
#  duration          :integer          not null
#  pay_period        :integer          not null
#  rate              :float            not null
#  back_payment_rate :float            not null
#  active            :boolean          default("true")
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class PaymentPlan < ApplicationRecord
  belongs_to :loan
  has_many :payments

  def root_payment_amount
    root_amount.fdiv(duration)
  end

  def percent_payment_amount
    (root_payment_amount * (rate - 1)).fdiv(12.fdiv(duration))
  end

  def percent_back_payment_amount
    (root_payment_amount * (back_payment_rate - 1)).fdiv(12.fdiv(duration))
  end

  def summary_payment_amount
    root_payment_amount + percent_payment_amount
  end

  def summary_back_payment_amount
    root_payment_amount + percent_back_payment_amount
  end

  def close_payment_amount
    loan.balance + percent_payment_amount
  end
end
