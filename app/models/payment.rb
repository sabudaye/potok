# == Schema Information
#
# Table name: payments
#
#  id                   :integer          not null, primary key
#  loan_id              :integer
#  company_id           :integer
#  summary_amount       :decimal(, )      not null
#  root_amount          :decimal(, )      not null
#  percent_amount       :decimal(, )      not null
#  period               :integer          not null
#  back_payment         :boolean          default("false")
#  partial_root_payment :boolean          default("false")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  payment_plan_id      :integer
#

class Payment < ApplicationRecord
  belongs_to :loan
  belongs_to :company
  belongs_to :payment_plan

  before_save :calculate_amounts

  private

  def calculate_amounts
    self.root_amount = payment_plan.root_payment_amount
    self.percent_amount = back_payment ? payment_plan.percent_back_payment_amount : payment_plan.percent_payment_amount
  end
end
