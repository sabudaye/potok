# == Schema Information
#
# Table name: loans
#
#  id         :integer          not null, primary key
#  company_id :integer
#  amount     :decimal(, )      not null
#  paid       :boolean          default("false")
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Loan < ApplicationRecord
  belongs_to :company
  has_many :payment_plans
  has_many :payments

  def balance
    amount - payments.sum(:root_amount)
  end
end
