Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'income_calculator#index'
  resources :income_calculator, only: [:index, :new], controller: :income_calculator
end
