# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160925142014) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loans", force: :cascade do |t|
    t.integer  "company_id"
    t.decimal  "amount",                     null: false
    t.boolean  "paid",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["company_id"], name: "index_loans_on_company_id", using: :btree
  end

  create_table "payment_plans", force: :cascade do |t|
    t.integer  "loan_id"
    t.decimal  "root_amount",                      null: false
    t.integer  "duration",                         null: false
    t.integer  "pay_period",                       null: false
    t.float    "rate",                             null: false
    t.float    "back_payment_rate",                null: false
    t.boolean  "active",            default: true
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["loan_id"], name: "index_payment_plans_on_loan_id", using: :btree
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "loan_id"
    t.integer  "company_id"
    t.decimal  "summary_amount",                       null: false
    t.decimal  "root_amount",                          null: false
    t.decimal  "percent_amount",                       null: false
    t.integer  "period",                               null: false
    t.boolean  "back_payment",         default: false
    t.boolean  "partial_root_payment", default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "payment_plan_id"
    t.index ["company_id"], name: "index_payments_on_company_id", using: :btree
    t.index ["loan_id"], name: "index_payments_on_loan_id", using: :btree
    t.index ["payment_plan_id"], name: "index_payments_on_payment_plan_id", using: :btree
  end

  add_foreign_key "loans", "companies"
  add_foreign_key "payment_plans", "loans"
  add_foreign_key "payments", "companies"
  add_foreign_key "payments", "loans"
  add_foreign_key "payments", "payment_plans"
end
