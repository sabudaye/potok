class CreatePaymentPlan < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_plans do |t|
      t.belongs_to :loan, index: true, foreign_key: true
      t.decimal :root_amount, null: false
      t.integer :duration, null: false
      t.integer :pay_period, null: false
      t.float :rate, null: false
      t.float :back_payment_rate, null: false
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
