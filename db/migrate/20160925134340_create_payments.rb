class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.belongs_to :loan, index: true, foreign_key: true
      t.belongs_to :company, index: true, foreign_key: true
      t.decimal :summary_amount, null: false
      t.decimal :root_amount, null: false
      t.decimal :percent_amount, null: false
      t.integer :period, null: false
      t.boolean :back_payment, default: false
      t.boolean :partial_root_payment, default: false

      t.timestamps
    end
  end
end
