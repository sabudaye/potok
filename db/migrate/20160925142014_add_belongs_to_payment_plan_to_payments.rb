class AddBelongsToPaymentPlanToPayments < ActiveRecord::Migration[5.0]
  def change
    add_reference :payments, :payment_plan, foreign_key: true 
  end
end
