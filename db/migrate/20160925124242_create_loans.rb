class CreateLoans < ActiveRecord::Migration[5.0]
  def change
    create_table :loans do |t|
      t.belongs_to :company, index: true, foreign_key: true
      t.decimal :amount, null: false
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end
