# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
DatabaseCleaner.clean_with :truncation
companies = Company.create(
  [
    { name: 'ЮЛ1' },
    { name: 'ЮЛ2' },
    { name: 'ЮЛ3' }
  ]
)

loans = Loan.create(
  [
    { company: companies.first, amount: 1_000_000, paid: true },
    { company: companies.second, amount: 1_000_000, paid: true },
    { company: companies.third, amount: 1_000_000, paid: true }
  ]
)

payment_plans = PaymentPlan.create(
  [
    { loan: loans.first, root_amount: loans.first.amount, duration: 6, pay_period: 1, rate: 1.3, back_payment_rate: 1.5 },
    { loan: loans.second, root_amount: loans.second.amount, duration: 6, pay_period: 1, rate: 1.3, back_payment_rate: 1.5 },
    { loan: loans.third, root_amount: loans.third.amount, duration: 6, pay_period: 1, rate: 1.3, back_payment_rate: 1.5 }
  ]
)

payments = Payment.create(
  [
    (1..6).map { |n| {
      loan: loans.first,
      company: companies.first,
      payment_plan: payment_plans.first,
      summary_amount: payment_plans.first.summary_payment_amount,
      period: n
    }},

    (1..3).map { |n| {
      loan: loans.second,
      company: companies.second,
      payment_plan: payment_plans.second,
      summary_amount: payment_plans.second.summary_payment_amount,
      period: n
    }},

    (1..2).map { |n| {
      loan: loans.third,
      company: companies.third,
      payment_plan: payment_plans.third,
      summary_amount: payment_plans.third.summary_payment_amount,
      period: n
    }},
    (3..6).map { |n| {
      loan: loans.third,
      company: companies.third,
      payment_plan: payment_plans.third,
      summary_amount: payment_plans.third.summary_back_payment_amount,
      back_payment: true,
      period: n
    }}
  ]
)

loans.second.reload
payment_plans.second.reload
Payment.create(
  loan: loans.second,
  company: companies.second,
  payment_plan: payment_plans.second,
  summary_amount: payment_plans.second.close_payment_amount,
  partial_root_payment: true,
  period: 4
)
