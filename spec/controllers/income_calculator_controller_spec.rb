require 'rails_helper'
describe IncomeCalculatorController, type: :controller do
  render_views

  describe "GET #new" do
    it "возвращает страницу без ошибок" do
      get :new, params: { investment_amount: 10000 }

      expect(response).to be_ok
    end
  end
end
