require 'rails_helper'
describe IncomeCalculator do
  describe "#calculate" do
    let(:investment_amount) { rand(10_000..100_000) }

    subject { described_class.calculate(investment_amount) }

    context 'оплата всех займов происходила вовремя' do
      let!(:companies) { create_list(:company, 3) }

      it 'считает ожидаемый и фактический доход' do
        expect(subject[:expected]).to eq(investment_amount * Settings.expected_income_rate)
        expect(subject[:actual]).to eq(investment_amount * Settings.expected_income_rate)
      end
    end
  end
end
