FactoryGirl.define do
  sequence(:integer) { |n| n }
  sequence(:name) { |n| "name#{n.to_s}" }
end
