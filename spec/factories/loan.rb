FactoryGirl.define do
  factory :loan do
    amount { rand(500_000..10_000_000) }

    trait :with_good_payments do
      transient do
        duration 6
        pay_period 1
        rate 1.3
        back_payment_rate 1.5
      end

      after(:create) do |loan, evaluator|
        create(
          :payment_plan_with_good_payments,
          loan: loan,
          root_amount: loan.amount,
          duration: evaluator.duration,
          pay_period: evaluator.pay_period,
          rate: evaluator.rate,
          back_payment_rate: evaluator.back_payment_rate
        )
      end
    end
  end
end
