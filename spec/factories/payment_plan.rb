FactoryGirl.define do
  factory :payment_plan do
    trait :with_good_payments do
      transient do
        payments_count 6
      end

      after(:create) do |payment_plan, evaluator|
        periods = (1..evaluator.payments_count).to_a
        create_list(
          :payment,
          evaluator.payments_count,
          period: periods.shift,
          summary_amount: payment_plan.root_amount.fdiv(evaluator.payments_count) * payment_plan.rate,
          payment_plan: payment_plan,
          loan: payment_plan.loan,
          company: payment_plan.loan.company
        )
      end
    end

    factory :payment_plan_with_good_payments, traits: [:with_good_payments]
  end
end
