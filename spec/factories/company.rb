FactoryGirl.define do
  factory :company do
    name { generate :name }

    after(:create) do |company|
      create(:loan, :with_good_payments, company: company)
    end
  end
end
